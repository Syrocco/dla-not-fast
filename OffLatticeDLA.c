#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#define method 1



typedef struct cp gridInfo;
struct cp{
	int number;
	int *tab;	
};

int testNearby(float **array, float position[], gridInfo gridBox);
void savePointToGrid(float x, float y, int index, int i, int j, gridInfo **grid, int boxSize, int boxNumber);
void coordToTXT(int arraySize, float **array, int info);
float** boundaryInitialisation(int arraySize);
gridInfo** gridInitialisation(int boxNumber, int gridSize, int boxSize);
void decompactify(float x1, float y1, float position[]);
void update(gridInfo *structure);
void randomPointOnCircle(float rad, float position[]);
float evolution(float **array, float maxRad, int step, gridInfo** grid, int boxSize, int boxNumber, int gridSize);
int iteration(float **array, int nbIterations, gridInfo** grid, int boxSize, int boxNumber, int gridSize);
void coordToIndex(int *i, int *j, float x, float y, int gridSize, int boxSize);
void addPoint(gridInfo *structure, int index);
void printGrid(gridInfo** grid, int boxNumber);



int main(int argc, char *argv[]){
	srand(time(NULL));
	
	int nbParticles = 6000; 
	
	
	int boxNumber = 500; //boxNumber^2 = nombre de cases. à garder pair pour l'instant
	int boxSize = 6; //longueur d'une box
	int gridSize = boxNumber*boxSize;
	
	
	gridInfo **grid = gridInitialisation(boxNumber, gridSize, boxSize);
    float **tab = boundaryInitialisation(nbParticles);
	
	int infoEnd = iteration(tab, nbParticles, grid, boxSize, boxNumber, gridSize);
	
	if (infoEnd){
		printf("Pas totalement fini, mais pas d'erreur :^(\n");
	}
	else{
		printf("Fini\n");
	}
	
	coordToTXT(nbParticles, tab, infoEnd);
	

	return 0;
	
}






int testNearby(float **array, float position[], gridInfo gridBox){
	
	int a = gridBox.number;
	for (int i = 0; i < a; i++){
		if ((position[0]-array[(gridBox.tab)[i]][0])*(position[0]-array[(gridBox.tab)[i]][0])+(position[1]-array[(gridBox.tab)[i]][1])*(position[1]-array[(gridBox.tab)[i]][1])<=4){
			decompactify(array[(gridBox.tab)[i]][0], array[(gridBox.tab)[i]][1], position);
			return 1;
		}
	}
	return 0;
	
}

void savePointToGrid(float x, float y, int index, int i, int j, gridInfo **grid, int boxSize, int boxNumber){


	addPoint(&grid[i][j], index);
	int boundary[2] = {0,0};
	
	if ( ((fabs(fmodf(x, boxSize)) < 1) && (x>=0)) || ((fabs(fmodf(x, boxSize)) > boxSize-1) && (i > 0) && (x<=0)) ){

		addPoint(&grid[i-1][j], index);
		boundary[0] = -1;
		
	}
	if ( ((fabs(fmodf(x, boxSize)) > boxSize-1) && (i < boxNumber-1) && (x>=0)) || ((fabs(fmodf(x, boxSize)) < 1) && (x<=0)) ){
		addPoint(&grid[i+1][j], index);
		boundary[0] = 1;
		
	}
	if ( ((fabs(fmodf(y, boxSize)) < 1) && (y>=0)) || ((fabs(fmodf(y, boxSize)) > boxSize-1) && (j<boxNumber-1) && (y<=0)) ){
		addPoint(&grid[i][j+1], index);
		boundary[1] = 1;
		
	}
	if ( ((fabs(fmodf(y, boxSize)) > boxSize-1) && (j > 0) && (y>=0)) || ((fabs(fmodf(y, boxSize)) < 1) && (y<=0)) ){
		addPoint(&grid[i][j-1], index);
		boundary[1] = -1;
	
	}

	// Le test est trop large et inclue des particules sur les diagonales qu'il ne faudrait pas inclure. Mais c'est pas grave.
	if (boundary[0] != 0 && boundary[1] != 0 ){ 
		addPoint(&grid[i+boundary[0]][j+boundary[1]], index);
	}


}



void printGrid(gridInfo** grid, int boxNumber){
	for (int i = 0; i < boxNumber; i++){
		for (int j = 0; j < boxNumber; j++){
			printf("%d\t", grid[j][i].number);
		}	
		printf("\n");
	}
}



void addPoint(gridInfo *structure, int index){
	structure->number += 1; //Change le nombre de points
	structure->tab = realloc(structure->tab, structure->number*sizeof(int));  //augmente dynamiquement la mémoire allouée à tab qui contient les index des points (fort heuresement fonctionne comme malloc si (*structure).tab=NULL).
	(structure->tab)[structure->number - 1] = index; //stocke dans le tableau l'index de la particule ajoutée

}


gridInfo** gridInitialisation(int boxNumber, int gridSize, int boxSize){
	gridInfo** array = malloc(boxNumber*sizeof(gridInfo *));
	for (int i = 0; i < boxNumber; i++){
		array[i] = malloc(boxNumber*sizeof(gridInfo));
	}	
	
	for (int i = 0; i < boxNumber; i++){
		for (int j = 0; j < boxNumber; j++){
			array[i][j].number = 0;
			array[i][j].tab = NULL;
		}
	}
	
	int i,j;
	float x = 0.001, y = 0.001;
	
	coordToIndex(&i, &j,  x,  y,  gridSize,  boxSize);  //la fonction SavePointToGrid necessiterait plus de condition pour attribuer correctement les pts x = y = 0, comme ils n'arrivent jamais, je préfère faire une exception ici que modifier savePointToGrid
	savePointToGrid(y, x, 1, i, j, array, boxSize, boxNumber);
	
	return array;
}

void update(gridInfo *structure){ //Augmente de 1 le nombre de points pouvant être contenus dans la structure.
	structure->number += 1; //Change le nombre de points
	structure->tab = realloc(structure->tab, structure->number*sizeof(int));  //augmente dynamiquement la mémoire allouée à tab qui contient les index des points (fort heuresement fonctionne comme malloc si (*structure).tab=NULL).
	*(structure->tab + structure->number - 1) = structure->number; 
}

void coordToIndex(int *i, int *j, float x, float y, int gridSize, int boxSize){
	*i = (gridSize/2 + x)/boxSize;
	*j = (-y + gridSize/2)/boxSize;
}




void coordToTXT(int arraySize, float **array, int info){
	FILE *fichier;
	fichier=fopen("tab.txt","w");
 	
	fprintf(fichier,"%d %d\n", info, info);
	
	for(int i=1; i<arraySize; i++){
		for (int j=0; j<2; j++){
			fprintf(fichier,"%f ",array[i][j]);
		}
		fprintf(fichier,"\n");
    }
	fclose(fichier);
}


float** boundaryInitialisation(int arraySize){
	
	float **array = (float **)malloc(arraySize * sizeof(float *)); 
    for (int i=0; i<arraySize; i++) 
         array[i] = (float *)calloc(2, sizeof(float));
	
	return array;
}


void randomPointOnCircle(float rad, float position[]){ //prend une position random sur un cercle de rayon rad et de centre middle
	float theta=((float)rand()/(float)(RAND_MAX))*2*M_PI;
	position[0]=rad*cos(theta);
	position[1]=rad*sin(theta);
}


float evolution(float **array, float maxRad, int step, gridInfo** grid, int boxSize, int boxNumber, int gridSize){
	float position[2];
	float move[2];
	float rad;
	int i,j;
	randomPointOnCircle(maxRad, position);
	while (1){

		randomPointOnCircle(1,move);
		position[0]+=move[0];
		position[1]+=move[1];
		
		rad=sqrt(position[0]*position[0]+position[1]*position[1]);
		
		

		if ( (rad > (maxRad+5)*2) || (rad > gridSize/2-4) ){
			randomPointOnCircle(maxRad, position);
		}
		
		
		
		if (rad<maxRad+10){

			
			coordToIndex(&i, &j,  position[0],  position[1],  gridSize,  boxSize);

			
			if (testNearby(array, position, grid[i][j])){
				array[step][0]=position[0];
				array[step][1]=position[1];
				coordToIndex(&i, &j,  position[0],  position[1],  gridSize,  boxSize); //pour mettre le decompactifier
				savePointToGrid(position[0], position[1], step, i, j, grid, boxSize, boxNumber);

				
				return rad; //ce n'est pas le bon radius comme j'ai legerement deplacé la particule, mais il ne changera pas de bcp, alors cela reste correct
			}
		}
	}		
}


int iteration(float **array, int nbIterations, gridInfo** grid, int boxSize, int boxNumber, int gridSize){
	float radiusParameter=0;
	float radiusBuffer;
	for (int i=1;i<nbIterations;i++){ 
		if (i%(int) (nbIterations/10)==0){
			printf("%d/%d\n",i,nbIterations);
		}
		
		radiusBuffer=evolution(array, radiusParameter+5, i, grid, boxSize, boxNumber, gridSize); //stock la distance au centre de la particule qui vient d'etre ajoutée à l'arbre
		if (radiusBuffer>radiusParameter){
			radiusParameter=radiusBuffer; //Enregistre la distance au centre de la particule la + éloignée
			//printf("radiusParameter = %f\n", radiusParameter);
			if (radiusParameter + 30 > gridSize/2){
				return i;
			}
		}
	}
	return 0;
}






void decompactify(float x1, float y1, float position[]){ //La mort de l'inventivité :) :) :) Au moins, ca marche...
	
	float x2=position[0];
	float y2=position[1];
	float distance=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
	float angle=fabs(asin((x1-x2)/distance));
	
	if ((x1<=x2) && (y1>=y2)){
		position[0]+=(2-distance)*sin(angle);
		position[1]-=(2-distance)*cos(angle);
	}
	
	if ((x1>=x2) && (y1>=y2)){
		position[0]-=(2-distance)*sin(angle);
		position[1]-=(2-distance)*cos(angle);
	}
	
	if ((x1<=x2) && (y1<=y2)){
		position[0]+=(2-distance)*sin(angle);
		position[1]+=(2-distance)*cos(angle);
	}
	
	if ((x1>=x2) && (y1<=y2)){
		position[0]-=(2-distance)*sin(angle);
		position[1]+=(2-distance)*cos(angle);
	}

}











