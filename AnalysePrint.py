import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm 

cmap = matplotlib.cm.get_cmap('cool')



tab=np.loadtxt("tab.txt")
a,b = np.shape(tab)

if a==b:
    
    tab=tab.astype(int)
    
    
    cmap=plt.cm.cool
    cmap.set_bad(color='black',alpha=1)
    tab = np.ma.masked_where(tab == 0, tab)
    plt.imsave('OnLattice.png', tab, cmap=cmap,dpi=500)
    plt.imshow(tab,interpolation="none", cmap=cmap)


else:
    
    if int(tab[0,0]) == 0:
    	nbParticles = len(tab)
    else:
    	nbParticles = int(tab[0,0])
    	tab[0,:] = 0
    
    
    
    
    fig, ax = plt.subplots()
    fig.set_size_inches(12, 12)
    ax.patch.set_facecolor('black')
    
    
    for i in range(nbParticles):
    	circle=plt.Circle((tab[i,0],tab[i,1]),1,color=cmap(i/nbParticles))
    	ax.add_artist(circle)
    
    
    xmax=max(np.abs(np.max(tab[:,0])),np.abs(np.min(tab[:,0])))
    ymax=max(np.abs(np.max(tab[:,1])),np.abs(np.min(tab[:,1])))
    plt.xlim(-xmax-10,xmax+10)
    plt.ylim(-ymax-10,ymax+10)
    plt.xticks([],[])
    plt.yticks([],[])
    
    plt.savefig('OffLattice.png', edgecolor="black",dpi=300, bbox_inches='tight', pad_inches = 0)
    
    plt.show()

